using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioShim : MonoBehaviour
{
    public static void SetRTPCValue(string name, float value)
    {
        //AkSoundEngine.SetRTPCValue(name, value);
    }

    public static void SetRTPCValue(string name, float value, GameObject target)
    {
        //AkSoundEngine.SetRTPCValue(name, value, target);
    }

    public static void PostEvent(string name, GameObject target)
    {
        //AkSoundEngine.PostEvent(name, target);
    }

    public static void SetSwitchValue(string name, string newValue, GameObject target)
    {
        //AkSoundEngine.SetSwitch(name, newValue, target);
    }
}
