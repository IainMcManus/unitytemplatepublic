﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SceneLoader : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] UnityEngine.UI.Text ProgressText;

    [SerializeField] string SceneToLoad;

    [SerializeField] float ActivationDelay = 1f;
    [SerializeField] int ActivationDelayCycles = 5;

    [SerializeField] UnityEvent OnCanCompleteLoading;
    #pragma warning restore 0649

    protected bool CanSendLoadingEvent = false;
    protected bool HasSentLoadingEvent = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // send the loading event
        if (CanSendLoadingEvent && !HasSentLoadingEvent)
        {
            HasSentLoadingEvent = true;
            OnCanCompleteLoading?.Invoke();
        }
    }

    public void BeginSceneLoading()
    {
        StartCoroutine(PerformAsyncLoad(SceneToLoad));
    }

    IEnumerator PerformAsyncLoad(string sceneName)
    {
        yield return null;

        // start the scene loading but disable it being activated once done
        AsyncOperation loadAO = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        loadAO.allowSceneActivation = false;

        float previousProgress = 0f;

        // wait until the scene is done
        while (!loadAO.isDone)
        {
            // Remap the progress from 0 to 100%
            float progress = Mathf.Clamp01(loadAO.progress / 0.9f);

            // progress has increased
            if (progress > previousProgress)
            {
                previousProgress = progress;

                ProgressText.text += ".";
            }

            // load finished?
            if (loadAO.progress == 0.9f)
            {
                CanSendLoadingEvent = true;

                // slight delay for activation
                for (int delayCycle = 0; delayCycle < ActivationDelayCycles; ++delayCycle)
                {
                    ProgressText.text += ".";
                    yield return new WaitForSeconds(ActivationDelay);
                }

                loadAO.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
