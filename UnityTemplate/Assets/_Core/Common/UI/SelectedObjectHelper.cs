﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectedObjectHelper : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] GameObject SelectOnEnable;
    #pragma warning restore 0649

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnEnable()
    {
        if (SelectOnEnable != null)
            EventSystem.current.SetSelectedGameObject(SelectOnEnable);
    }
}
